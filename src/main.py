# Ahmad Qadri (arqchicago@gmail.com)

from levenshtein import levenshtein

lev_d, lev_mat = levenshtein('Vermont', 'Virginia', case=False)

print "distance:\n",
print lev_d 
print
print "matrix:\n",
print lev_mat 
print

lev_d, lev_mat = levenshtein('HONDA', 'HYUNDAI', case=False)

print "distance:\n",
print lev_d 
print
print "matrix:\n",
print lev_mat 
print

lev_d, lev_mat = levenshtein('Honda', 'hyuNDai', case=True)

print "distance:\n",
print lev_d 
print
print "matrix:\n",
print lev_mat 
print