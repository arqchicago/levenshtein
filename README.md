# Levenshtein Distance

## What is Levenshtein Distance?
Levenshtein Distance (LD) is a metric that is used to compare two strings. It is a measure of similarity between them. 
The "distance" in "Levenshtein Distance" refers to the number of insertions, substitutions or deletions that are 
required to convert one string to another. This metric is also sometimes known as edit distance. This similarity 
metric is named after Vladimir Levenshtein who came up with this metric in 1965. 

## Examples

LD between "Vermont" and "Virginia" is 5.

* Substitute "e" with "i"
* Substitute "m" with "g"
* Substitute "o" with "i"
* Substitute "t" with "i"
* Insert "a"

LD between "HONDA" and "HYUNDAI" is 3.

* Substitute "O" with "Y"
* Insert "U"
* Insert "I"

LD based on case sensitive comparison between "Honda" and "hyuNDai" is 6.

* Substitute "H" with "h"
* Substitute "o" with "y"
* Insert "u"
* Substitute "n" with "N"
* Substitute "d" with "D"
* Insert "i"

## Function Usage
Function levenshtein is programmed in levenshtein.py. File main.py shows examples of how levenshtein function should be called. 
The function takes as input the source string, target string and a boolean case (True if case sensitive comparison is required, 
false otherwise). The function returns the distance as the first argument and the associated levenshtein matrix as the second
argument.

Here are some examples.

```sh
lev_d, lev_mat = levenshtein('Vermont', 'Virginia', case=False)
lev_d, lev_mat = levenshtein('HONDA', 'HYUNDAI', case=False)
lev_d, lev_mat = levenshtein('Honda', 'hyuNDai', case=True)
```